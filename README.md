# Notas sobre o Git

Criação do projeto:

	$ git init

Adição de arquivo para ser monitorado:

	$ git add <arquivo>

Obter informações sobre o projeto:

	$ git status

Salvar nova versão:

	$ git commit -m 'Descrição da nova versão'

Para listar as versões:

	$ git log

	$ git log --oneline --decorate --all

Para alterar a posição atual:

	$ git checkout <versão>

Para descartar alterações:

	$ git checkout HEAD -- <arquivo>

Para guardar temporariamente alterações:

	$ git stash

Para recuperar essas alterações guardadas:

	$ git stash apply

Nova alteração!!!
